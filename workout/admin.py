from django.contrib import admin
from .models import Workout

# Register your models here.

class WorkoutAdmin(admin.ModelAdmin):
    readonly_fields = [
        'bmi', 'state_bmi', 'published'
    ]

admin.site.register(Workout, WorkoutAdmin)