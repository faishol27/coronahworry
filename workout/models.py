from django.db import models

# Create your models here.

class Workout(models.Model):
    TIME = (
        ('0-30 menit', '1'),
        ('30 menit - 1 jam','2'),
        ('1 - 1.5 jam', '3'),
        ('1.5 - 2 jam', '4'),
    )
    surname         = models.CharField(max_length=20)
    work_activity   = models.CharField(max_length=50)
    duration        = models.CharField(max_length=20, choices=TIME, default='0-30 menit')
    description     = models.TextField()
    weight          = models.FloatField()
    height          = models.FloatField()
    bmi             = models.FloatField(null=True)
    state_bmi       = models.CharField(max_length=20, editable=False)
    published       = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{}. {}, work={}".format(self.id, self.surname, self.work_activity)