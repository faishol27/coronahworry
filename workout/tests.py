from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Workout
from .views import create_work, list_work, all_work, detail_work

# Create your tests here.

class WorkoutTest(TestCase):
    
    def setUp(self):
        self.client = Client()
        self.create_work_url = reverse('workout:create')
        self.test_create_work = Workout.objects.create(
            surname         = "vanni",
            work_activity   = "Watching Virtual Concert",
            duration        = "0-30 menit",
            description     = "very intersting activity, u have to try it whuaa",
            weight          = "47",
            height          = "1.55",
        )

    #test url
    def test_workout_url_is_exist(self):
        response = Client().get('/workout/')
        self.assertEqual(response.status_code, 200)

    def test_all_workout_is_exist(self):
        response = Client().get('/workout/all')
        self.assertEqual(response.status_code, 301)

    #test view
    def test_workout_using_create_func(self):
        found = resolve('/workout/')
        self.assertEqual(found.func, create_work)

    def test_all_workout_using_all_work_func(self):
        found = resolve('/workout/all/')
        self.assertEqual(found.func, all_work)
    
    def test_list_work_using_list_work_func(self):
        found = resolve('/workout/list/')
        self.assertEqual(found.func, list_work)

    def test_detail_work_using_detail_work_func(self):
        found = resolve('/workout/detail/1')
        self.assertEqual(found.func, detail_work)

   #test template
    def test_workout_using_index_template(self):
        response = Client().get('/workout/')
        self.assertTemplateUsed(response, 'workout/index.html')

    def test_workout_using_list_template(self):
        response = Client().get('/workout/list/')
        self.assertTemplateUsed(response, 'workout/list.html' )

    #test model
    def test_can_save_workout_a_POST_request(self):
        new_workout = Workout.objects.create(
            surname         = "vanni",
            work_activity   = "Watching Virtual Concert",
            duration        = "0-30 menit",
            description     = "very intersting activity, u have to try it whuaa",
            weight          = "47",
            height          = "1.55",
        )

        count_all_available_workout = Workout.objects.all().count()
        self.assertEqual(count_all_available_workout, 2)