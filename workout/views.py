from django.shortcuts import render, redirect
from .models import Workout
from .forms import WorkoutForm
from django.core.paginator import Paginator
import math

# Create your views here.
def create_work(request):
    work_form = WorkoutForm()

    context = {}
    if request.method == "POST":
        surname         = request.POST.get("surname")
        work_activity   = request.POST.get("work_activity")
        duration        = request.POST.get("duration")
        description     = request.POST.get("description")
        weight          = request.POST.get("weight")
        height          = request.POST.get("height")

        if weight :
            weight = float(request.POST.get("weight"))
            height = float(request.POST.get("height"))

        bmi = (weight/(height**2))

        print(bmi)

        state_bmi = ""
        save  = "on"

        #Kondisi untuk state BMI (range)
        if save == "on":
            if (bmi<18.5):
                state_bmi="Underweight"
            elif (bmi>=18.5 and bmi<=24.9):
                state_bmi="Normal"
            elif (bmi>=25 and bmi<=29.9):
                state_bmi="Preobese"
            elif (bmi>=30 and bmi<=34.9):
                state_bmi="Obese Class I"
            elif (bmi>=35 and bmi<=39.9):
                state_bmi="Obese Class II"
            elif (bmi>=40):
                state_bmi="Obese Class III"

            Workout.objects.create(
                surname=surname, 
                work_activity=work_activity, 
                duration=duration,
                description=description, 
                weight=weight,
                height=height,
                bmi=round(bmi, 2),
                state_bmi=state_bmi
                )
            
            return redirect('workout:list')

        context['bmi']          = bmi
        context['state_bmi']    = state_bmi,
        context['work_form']    = work_form

    return render(request, 'workout/index.html', context)

def list_work(request):
    all_work    = Workout.objects.all()
    latest      = all_work[len(all_work)-1] #Mengambil objek yang baru ditambahkan
    a_latest    = latest.state_bmi
    related     = Workout.objects.filter(state_bmi=a_latest).order_by('-published')
    n_related   = len(related)

    works_per_page = 6
    paginator   = Paginator(related, works_per_page)
    page_number = request.GET.get('page')
    page_obj    = paginator.get_page(page_number)
    

    context = {
        'all_work'      : all_work,
        'a_latest'      : a_latest,
        'related'       : related,
        'page_obj'      : page_obj,
        'works_per_page': works_per_page,
        'n_related'     : n_related,
    }

    print(related)
    print(len(page_obj))

    return render(request, 'workout/list.html', context)

def all_work(request):
    all_work    = Workout.objects.all().order_by('-published')
    n_all_work  = len(all_work)

    works_per_page = 6
    paginator   = Paginator(all_work, works_per_page)
    page_number = request.GET.get('page')
    page_obj    = paginator.get_page(page_number)

    context = {
        'all_work'      : all_work,
        'page_obj'      : page_obj,
        'works_per_page': works_per_page,
        'n_all_work'    : n_all_work,
    }
    print(n_all_work)

    return render(request, 'workout/all.html', context)

def detail_work(request, detail_id):
    work_detail = Workout.objects.get(id=detail_id)

    print(work_detail.bmi)
    context = {
        'work_detail' : work_detail,

    }
    return render(request, 'workout/detail.html', context)