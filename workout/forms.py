from django import forms

class WorkoutForm(forms.Form):
    
    surname         = forms.CharField(max_length=20)
    work_activity   = forms.CharField(max_length=50)
    duration        = forms.CharField(max_length=20)
    description     = forms.CharField(max_length=200, widget=forms.Textarea)
    weight          = forms.FloatField()
    height          = forms.FloatField()
    
