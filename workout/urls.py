from django.urls import path
from .views import create_work, list_work, detail_work, all_work
from django.conf.urls import url

app_name = 'workout'

urlpatterns = [
    path('', create_work, name="create"),
    path('all/', all_work, name="all"),
    url(r'^list/$', list_work, name="list"),
    url(r'^detail/(?P<detail_id>[0-9]+)$', detail_work, name="detail"),
]