from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver
from django.contrib.auth.models import User

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:index'))
        self.assertEqual(response.status_code, 200)

    def test_logout_url(self):
        RED_CHAIN = [('/login/', 302)]
        resp = self.client.get('/logout/', follow = True)
        self.assertEquals(resp.redirect_chain, RED_CHAIN)

    def test_login_url_get_noauth(self):
        resp = self.client.get('/login/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'main/login.html')

    def test_register_url_get_noauth(self):
        resp = self.client.get('/register/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'main/register.html')

    def test_login_url_post_invalid(self):
        resp = self.client.post('/login/', {'username': 'asdf', 'lol': 'ikimasyo'})
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'main/login.html')

    def test_register_url_post_invalid(self):
        resp = self.client.post('/register/', {'username': 'asdf', 'lol': 'ikimasyo'})
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'main/register.html')

    def test_login_url_post_valid(self):
        User.objects.create_user(username = 'test', password = 'Halo1234')
        RED_CHAIN = [('/', 302)]
        resp = self.client.post('/login/', {'username': 'test', 'password': 'Halo1234'}, follow = True)
        self.assertEqual(resp.redirect_chain, RED_CHAIN)

    def test_register_url_post_valid(self):
        RED_CHAIN = [('/login/', 302)]
        resp = self.client.post('/register/', {'username': 'test', 'password1': 'Halo12345678', 'password2': 'Halo12345678'}, follow = True)
        self.assertEquals(resp.redirect_chain, RED_CHAIN)

    def test_login_url_auth(self):
        User.objects.create_user(username = 'test', password = 'Halo1234')
        self.client.login(username = 'test', password = 'Halo1234')
        RED_CHAIN = [('/', 302)]

        resp = self.client.get('/login/', follow = True)
        self.assertEquals(resp.redirect_chain, RED_CHAIN)
    
    def test_register_url_auth(self):
        User.objects.create_user(username = 'test', password = 'Halo1234')
        self.client.login(username = 'test', password = 'Halo1234')
        RED_CHAIN = [('/', 302)]

        resp = self.client.get('/register/', follow = True)
        self.assertEquals(resp.redirect_chain, RED_CHAIN)

class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
