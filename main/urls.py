from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.akun_login, name='login'),
    path('logout/', views.akun_logout, name='logout'),
    path('register/', views.akun_register, name='register'),
]
