from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Profile(models.Model):
    nama = models.CharField(max_length=50,  null=True, blank=True)
    umur  = models.IntegerField(null=True, blank=True)
    domisili = models.CharField(max_length=50, null=True, blank=True)
    berat_badan = models.IntegerField(null=True, blank=True)
    tinggi_badan = models.IntegerField(null=True, blank=True)
    user = models.OneToOneField(User, on_delete = models.CASCADE)

