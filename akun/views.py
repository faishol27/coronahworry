from django.shortcuts import render, redirect
from .models import Profile
from .forms import UserForm


# Create your views here.
def profile(request): 
    return redirect('akun:profile_dua')
    

def profile_dua(request): # ambil data user
    get_data_dua = None
    try:
        get_data_dua = Profile.objects.get(user = request.user)
    except:
        get_data_dua = Profile.objects.create(user = request.user)

    context = {'data':get_data_dua}
    return render(request, 'akun/hasilProfile.html', context)

def update_profile(request, pk): #update data user
    get_data_dua = Profile.objects.get(id=pk)
    get_profile = UserForm(instance = get_data_dua)
    
    if request.method == "POST":
        get_profile = UserForm(request.POST, instance=get_data_dua)
        if get_profile.is_valid():
            get_profile.edit_obj(get_data_dua)
            return redirect('akun:profile_dua')
    context = {'edit':get_profile}
    return render(request, 'akun/editProfile.html', context)
