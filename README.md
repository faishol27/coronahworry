# Coronahworry

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

## Daftar isi

- [Daftar isi](#daftar-isi)
- [Latar Belakang Aplikasi](#latar-belakang)
- [Anggota Kelompok](#anggota-kelompok)
- [Daftar Fitur](#daftar-fitur) 
- [Link Herokuapp](#link-herokuapp)

## Latar Belakang
Berdiam diri di rumah di masa pandemi menyebabkan banyak rasa jenuh yang muncul. Tidak hanya itu, pekerjaan yang tetap menumpuk membuat tingkat stress tiap orang bertambah. Dengan adanya web ini, diharapkan setiap orang dapat meluapkan stressnya dan memperbaiki kualitas hidupnya. Selain itu, adanya assessment corona dapat membuat orang-orang lebih sadar akan bahaya corona di sekitarnya.

## Anggota Kelompok
1. 1606875831 - Sukma Firdaus Suryahadi
2. 1906298941 - Josias Marchellino Pakpahan
3. 1906299080 - Puspita Sari Matovanni
4. 1906285573 - Muhammad Faishol Amirul Mukminin
5. 1906302900 - Muhammad Aditya Pratama Dungga

## Daftar Fitur
Daftar fitur yang akan diimplementasikan pada web ini sebagai berikut:
1. Post curhat - setiap user dapat melakukan post dan melihat curhat
2. Reply dan like curhat - setiap user dapat melakukan reply pada curhat dan melakukan like pada curhat maupun komentar
3. Corona Assessment - setiap user dapat melakukan assessment untuk probabilitas terkena coronavirus
4. Workout Share Media - setiap user dapat melakukan share program olahraga ringan
5. User Profile Management - setiap user dapat mengganti nama, domisili, dan info pribadi lainnya

## Link Herokuapp
Repository ini akan dideploy di [https://coronahworry.herokuapp.com](https://coronahworry.herokuapp.com)

[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://github.com/laymonage/django-template-heroku/commits/master
[pipeline-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/laymonage/django-template-heroku/-/commits/master
