from django.shortcuts import render, redirect
from assessment.models import Assessment
from assessment.forms import AssessmentForm

# Create your views here.


def index(request):
    if request.user.is_authenticated:
        data = Assessment.objects.filter(user=request.user)
        if(len(data) == 0):
            form = AssessmentForm(request.POST or None)
            if form.is_valid():
                form = form.save(commit=False)
                form.user = request.user
                form.save()
                return redirect('assessment:result')

            context = {
                'form': form
            }
            return render(request, 'assessment/assessment.html', context)
        else:
            if(data[0].user == request.user):
                return redirect('assessment:result')
    else:
        return redirect('main:login')


def retake(request):
    if request.user.is_authenticated:
        data = Assessment.objects.get(user=request.user)
        if request.method == "POST":
            data.delete()
            return redirect('assessment:index')


def result(request):
    data = Assessment.objects.filter(user=request.user)
    if(len(data) != 0):
        counterGejala = 0
        if(data[0].suhu == True):
            counterGejala += 1
        if(data[0].sesak == True):
            counterGejala += 1
        if(data[0].batuk == True):
            counterGejala += 1

        if(counterGejala == 0):
            if(data[0].kontak_gejala == True and data[0].kontak_positif == True):
                return render(request, 'assessment/medium.html')
            elif(data[0].kontak_gejala == True and data[0].kontak_positif == False):
                return render(request, 'assessment/low.html')
            elif(data[0].kontak_gejala == False and data[0].kontak_positif == True):
                return render(request, 'assessment/low.html')
            else:
                return render(request, 'assessment/low.html')

        elif(counterGejala == 1):
            if(data[0].kontak_gejala == True and data[0].kontak_positif == True):
                return render(request, 'assessment/medium.html')
            elif(data[0].kontak_gejala == True and data[0].kontak_positif == False):
                return render(request, 'assessment/medium.html')
            elif(data[0].kontak_gejala == False and data[0].kontak_positif == True):
                return render(request, 'assessment/medium.html')
            else:
                return render(request, 'assessment/low.html')

        else:
            if(data[0].kontak_gejala == True and data[0].kontak_positif == True):
                return render(request, 'assessment/high.html')
            elif(data[0].kontak_gejala == True and data[0].kontak_positif == False):
                return render(request, 'assessment/medium.html')
            elif(data[0].kontak_gejala == False and data[0].kontak_positif == True):
                return render(request, 'assessment/high.html')
            else:
                return render(request, 'assessment/medium.html')
    else:
        return render('assessment:index')
