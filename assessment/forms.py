from django import forms
from assessment.models import Assessment


class AssessmentForm(forms.ModelForm):
    class Meta:
        model = Assessment
        fields = {
            'suhu',
            'batuk',
            'kontak_positif',
            'kontak_gejala',
            'sesak'
        }
