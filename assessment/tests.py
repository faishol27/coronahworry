from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve
from assessment.views import index, retake, result
from assessment.models import Assessment
from django.apps import apps
from assessment.apps import AssessmentConfig
# Create your tests here.


class AssessmentTest(TestCase):
    client = Client()

    def setUp(self):
        User.objects.create_user(
            username='test', first_name='Akun', last_name='Testing', password='HaloHalo12345678')
        user = User.objects.get(pk=1)
        self.client.login(username='test', password='HaloHalo12345678')

    def test_apps(self):
        self.assertEqual(AssessmentConfig.name, 'assessment')
        self.assertEqual(apps.get_app_config('assessment').name, 'assessment')

    def test_assessment_url_is_exist(self):
        response = self.client.get('/assessment/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_assessment_result_url_is_exist(self):
        user = User.objects.get(pk=1)
        Assessment.objects.create(user=user,
                                  suhu=True,
                                  sesak=True,
                                  batuk=True,
                                  kontak_positif=True,
                                  kontak_gejala=True)
        response = self.client.get('/assessment/result', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_assessment_index_using_right_template(self):
        response = self.client.get('/assessment/')
        self.assertTemplateUsed(
            response, 'assessment/assessment.html')

    def test_assessment_result_using_right_template(self):
        user = User.objects.get(pk=1)
        Assessment.objects.create(user=user,
                                  suhu=True,
                                  sesak=True,
                                  batuk=True,
                                  kontak_positif=True,
                                  kontak_gejala=True)
        response = self.client.get('/assessment/result', follow=True)
        self.assertTemplateUsed(
            response, 'assessment/high.html')

    def test_assessment_using_index_func(self):
        found = resolve('/assessment/')
        self.assertEqual(found.func, index)

    def test_assessment_using_result_func(self):
        found = resolve('/assessment/result/')
        self.assertEqual(found.func, result)

    def test_create_assessment(self):
        user = User.objects.get(pk=1)
        Assessment.objects.create(user=user,
                                  suhu=True,
                                  sesak=True,
                                  batuk=True,
                                  kontak_positif=True,
                                  kontak_gejala=True)
        counting_all_available_assessment = Assessment.objects.all().count()
        self.assertEqual(counting_all_available_assessment, 1)
