from django.contrib import admin
from assessment.models import Assessment


class AssessmentAdmin(admin.ModelAdmin):
    list_display = ('user', 'suhu', 'sesak', 'batuk',
                    'kontak_positif', 'kontak_gejala')
    list_filter = ('user', 'suhu', 'sesak', 'batuk',
                   'kontak_positif', 'kontak_gejala')
    search_fields = ('user', 'suhu', 'sesak', 'batuk',
                     'kontak_positif', 'kontak_gejala')

    class Meta:
        model = Assessment


admin.site.register(Assessment, AssessmentAdmin)
