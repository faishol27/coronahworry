from django.urls import path

from . import views

app_name = 'Postingan'

urlpatterns = [
    path('', views.post_list, name='index'),
]