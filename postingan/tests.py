from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .models import Post
from .views import post_list

# Create your tests here.

#Check URL
class Checkurl(TestCase):
    def test_index_url_exist(self):
        response = Client().get("/curhat/")
        self.assertEqual(response.status_code, 200)

#Check Views
class Checkviews(TestCase):
    def test_post_list_exist(self):
        response = resolve("/curhat/")
        self.assertEqual(response.func, post_list)

#Check Templates
class CheckTemplates(TestCase):
    def test_index_using_template(self):
        response = Client().get("/curhat/")
        self.assertTemplateUsed(response, "post/post.html")