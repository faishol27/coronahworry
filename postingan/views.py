from django.shortcuts import render,redirect
from .models import Post
from .forms import Postform

# Create your views here.
def post_list(request):
    posts = Post.objects.all()
    id = request.user
    form = Postform(initial={'user': id})
    if request.method == 'POST':
        form = Postform(request.POST)
        if form.is_valid():
            form.save()
            return redirect('postingan:index')
    context = {'post':posts, 'form': form}
    return render(request, "post/post.html", context)