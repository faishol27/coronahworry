from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

# Create your models here.
class Post(models.Model):
    message = models.TextField()
    time = models.DateTimeField(default=datetime.now, blank=True)
    user = models.ForeignKey(User, on_delete = models.CASCADE, default = None)