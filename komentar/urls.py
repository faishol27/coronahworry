from django.urls import include, path
from komentar.views import index, post_like

urlpatterns = [
    path('<int:postid>/view/', index, name = 'index'),
    path('<int:postid>/like/', post_like, name = 'like'),
]
