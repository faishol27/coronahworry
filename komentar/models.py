from django.contrib.auth.models import User
from django.db import models
from postingan.models import Post as Postingan

class Komentar(models.Model):
    postingan = models.ForeignKey(Postingan, on_delete = models.CASCADE)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    content = models.CharField(max_length = 200)

class Like(models.Model):
    postingan = models.ForeignKey(Postingan, on_delete = models.CASCADE)
    user = models.ForeignKey(User, on_delete = models.CASCADE)
