from django.test import TestCase, Client
from django.contrib.auth.models import User
from komentar.models import Komentar, Like
from postingan.models import Post as Postingan

class KomentarLoginTest(TestCase):
    client = Client()

    def setUp(self):
        User.objects.create_user(username = 'test', first_name = 'Akun', last_name = 'Testing', password = 'HaloHalo12345678')
        user = User.objects.get(pk = 1)
        Postingan.objects.create(user = user, message = 'Halooo')
        self.client.login(username = 'test', password = 'HaloHalo12345678')
        
    def test_postingan_not_existed_like(self):
        REDIRECT_EXP = [('/curhat/', 302)]
        resp = self.client.get('/curhat/1000000/like/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)
        
        likes = Like.objects.all()
        self.assertEqual(likes.count(), 0)
    
    def test_postingan_existed_add_like(self):
        REDIRECT_EXP = [('/curhat/1/view/', 302)]
        resp = self.client.get('/curhat/1/like/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)
        
        likes = Like.objects.all()
        self.assertEqual(likes.count(), 1)
    
    def test_postingan_existed_remove_like(self):
        user = User.objects.get(username = 'test')
        post = Postingan.objects.get(pk = 1)
        Like.objects.create(user = user, postingan = post)

        REDIRECT_EXP = [('/curhat/1/view/', 302)]
        resp = self.client.get('/curhat/1/like/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)
        
        likes = Like.objects.all()
        self.assertEqual(likes.count(), 0)

    def test_postingan_not_existed_komentar(self):
        REDIRECT_EXP = [('/curhat/', 302)]
        resp = self.client.get('/curhat/1000000/view/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)

    def test_postingan_exist_komentar_post_valid(self):
        resp = self.client.post('/curhat/1/view/', {'content': 'Ini komentar'})
        komentars = Komentar.objects.all()
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(komentars.count(), 1)
    
    def test_postingan_exist_komentar_post_invalid(self):
        resp = self.client.post('/curhat/1/view/', {'content': 'A' * 250})
        komentars = Komentar.objects.all()
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(komentars.count(), 0)

    def test_komentar_template(self):
        resp = self.client.get('/curhat/1/view/')
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'komentar.html')

class KomentarNonLoginTest(TestCase):
    def test_not_login_komentar(self):
        REDIRECT_EXP = [('/login/', 302)]
        resp = Client().get('/curhat/1/view/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)

    def test_not_login_like(self):
        REDIRECT_EXP = [('/login/', 302)]
        resp = Client().get('/curhat/1/like/', follow = True)
        self.assertEqual(resp.redirect_chain, REDIRECT_EXP)