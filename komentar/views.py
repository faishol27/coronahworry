from django.shortcuts import render, redirect
from komentar.models import Like, Komentar
from komentar.forms import KomentarForm
from postingan.models import Post as Postingan

def index(req, postid):
    if req.user.is_authenticated:
        try:
            context = {}

            komentarForm = KomentarForm()
            post = Postingan.objects.get(pk = postid)    
            
            if req.method == 'POST':
                komentarForm = KomentarForm(req.POST)
                if komentarForm.is_valid():
                    komentarForm.save(req.user, post)

            likes = Like.objects.filter(user = req.user).filter(postingan = post)
            
            context['postingan'] = post
            context['comments'] = Komentar.objects.filter(postingan = post)
            context['is_like'] = likes.count()
            context['form'] = komentarForm

            return render(req, 'komentar.html', context)
        except Exception as ex:
            return redirect('postingan:index')
    else:
        return redirect('main:login')

def post_like(req, postid):
    if req.user.is_authenticated:
        try:
            post = Postingan.objects.get(pk = postid)
            likes = Like.objects.filter(user = req.user).filter(postingan = post)
            if likes.count() > 0:
                likes.delete()
            else:
                Like.objects.create(user = req.user, postingan = post)
            return redirect('komentar:index', postid)
        except:
            return redirect('postingan:index')
    else:
        return redirect('main:login')